# fleet-infra

Kubernetes fleet infrastructure repository. This repository follow ArgoCD's [App of apps pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/) as a source of truth for cluster management.
